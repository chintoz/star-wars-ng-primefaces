import {Component} from '@angular/core';
import {Actor} from '../actors/store/actor.model';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.css']
})
export class ActorComponent {
  public actor: Actor;

  constructor(
    private http: HttpClient
  ) {
  }

  saveActor() {
    return this.http.post<Actor>('http://localhost:8080/actor', this.actor);
  }

  public setActor(actor: Actor) {
    this.actor = actor;
  }

}
