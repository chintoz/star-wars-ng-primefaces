export interface Actor {
    actorId: string;
    firstName: string;
    lastName: string;
    age: number;
    email: string;
    earned: number;
    currency: string;
    phone: string;
    money: string;
}
