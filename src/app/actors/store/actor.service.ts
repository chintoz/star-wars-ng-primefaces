import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as ActorActions from './actor.actions';
import {Actor} from './actor.model';
import * as ActorSelectors from './actor.selectors';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ActorService {
  constructor(private store: Store<Actor>) {}

  public loadActors() {
    this.store.dispatch(ActorActions.loadActors());
  }

  public addActor(actor: Actor) {
    this.store.dispatch(
      ActorActions.addActor({actor: {...actor}})
    );
  }

  public modifyActor(actor: Actor) {
    this.store.dispatch(
      ActorActions.modifyActor({actor: {...actor}})
    );
  }

  public deleteActor(actor: Actor) {
    this.store.dispatch(
      ActorActions.deleteActor({actor: {...actor}})
    );
  }

  public getActorsList$(): Observable<Actor[]> {
    return this.store.select(ActorSelectors.getActorsList);
  }

}


