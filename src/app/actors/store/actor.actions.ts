import {Actor} from './actor.model';
import {createAction, props} from '@ngrx/store';

export const loadActors = createAction(
  '[Actor] Load Actors'
);

export const loadActorsSucess = createAction(
  '[Actor] Load Actors Success',
  props<{ actors: Actor[] }>()
);

export const loadActorsError = createAction(
  '[Actor] Load Actors Error'
);

export const addActor = createAction(
  '[Actor] Add Actor',
  props<{ actor: Actor }>()
);

export const modifyActor = createAction(
  '[Actor] Modify Actor',
  props<{ actor: Actor }>()
);

export const deleteActor = createAction(
  '[Actor] Delete Actor',
  props<{ actor: Actor }>()
);
