import { createFeatureSelector, createSelector } from '@ngrx/store';
import { actorFeatureKey, ActorsState } from './actor.reducer';

export const getActorState = createFeatureSelector<ActorsState>(
  actorFeatureKey
);

export const getActorsList = createSelector(
  getActorState,
  (state: ActorsState) => state.actors
);


