import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, concatMap, map} from 'rxjs/operators';
import {initialState} from './actor.reducer';
import * as ActorActions from './actor.actions';
import {HttpClient} from '@angular/common/http';
import {Actor} from './actor.model';


@Injectable()
export class ActorEffects {
  constructor(private actions$: Actions, private http: HttpClient) {
  }

  public loadActors$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActorActions.loadActors),
      concatMap(() => {
        return this.http.get<Actor[]>('http://localhost:8080/actor').pipe(
          map(res => ActorActions.loadActorsSucess({actors: [...res]})),
          catchError(err => ActorActions.loadActorsError)
        );
      })
    ));

}
