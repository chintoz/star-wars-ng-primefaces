import {Actor} from './actor.model';
import {Action, createReducer, on} from '@ngrx/store';
import * as ActorActions from './actor.actions';

export const actorFeatureKey = 'actor';

export interface ActorsState {
  actors: Actor[];
}

export const initialState: ActorsState = {actors: []};

const actorReducer = createReducer(
  initialState,
  on(ActorActions.loadActors, state => state),
  on(ActorActions.loadActorsSucess, (state, { actors }) => ({actors: [...actors]})),
  on(ActorActions.loadActorsError, state => state),
  on(ActorActions.addActor, (state, {actor}) => ({actors: [...state.actors, actor]})),
  on(ActorActions.deleteActor, (state, {actor}) => ({actors: state.actors.filter(a => a.actorId === actor.actorId)})),
  on(ActorActions.modifyActor, (state, {actor}) => ({actors: state.actors.map(a => a.actorId === actor.actorId ? actor : a)}))
);

export function reducer(state: ActorsState | undefined, action: Action) {
  return actorReducer(state, action);
}
