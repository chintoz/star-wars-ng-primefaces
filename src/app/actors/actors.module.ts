import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import * as fromActorMethod from './store/actor.reducer';
import {ActorEffects} from './store/actor.effects';
import {EffectsModule} from '@ngrx/effects';
import {ActorsComponent} from './actors.component';
import {RouterModule, Routes} from '@angular/router';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import {FormsModule} from '@angular/forms';
import {PasswordModule} from 'primeng/password';
import {InputMaskModule} from 'primeng/inputmask';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';

const routes: Routes = [{ path: '', component: ActorsComponent }];

@NgModule({
  declarations: [ActorsComponent],
  imports: [
    PanelModule,
    InputTextModule,
    FormsModule,
    PasswordModule,
    ButtonModule,
    TableModule,
    InputMaskModule,
    HttpClientModule,
    CommonModule,
    RouterModule.forChild(routes),
    EffectsModule.forFeature([ActorEffects]),
    StoreModule.forFeature(
      fromActorMethod.actorFeatureKey,
      fromActorMethod.reducer
    )]
})
export class ActorsModule {}
