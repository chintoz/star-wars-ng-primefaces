import {Component, OnInit} from '@angular/core';
import {Actor} from './store/actor.model';
import {Router} from '@angular/router';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ActorComponent} from '../actor/actor.component';
import {ActorService} from './store/actor.service';


@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.css']
})
export class ActorsComponent implements OnInit {
  actors$: Observable<Actor[]>;
  cols: any[];

  constructor(
    private router: Router,
    private http: HttpClient,
    private actorService: ActorService
  ) {
  }

  ngOnInit(): void {
    this.actors$ = this.actorService.getActorsList$();
    /*
    this.getActors().subscribe(resp => {
      console.log(resp.body);
      this.actors = resp.body;
      console.log(this.actors);
    });
     */

    this.cols = [
      {field: 'firstName', header: 'First Name'},
      {field: 'lastName', header: 'Last Name'},
      {field: 'age', header: 'Age'},
      {field: 'email', header: 'Email'},
      {field: 'money', header: 'Money'},
      {field: 'phone', header: 'Phone'}
    ];
  }

  /*
  getActors(): Observable<HttpResponse<Actor[]>> {
    return this.http.get<Actor[]>(
      'http://localhost:8080/actor', {observe: 'response'});
  }
   */

  createActor() {
    this.router.navigate(['/actor']);
  }

}
