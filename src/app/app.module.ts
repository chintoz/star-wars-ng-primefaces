import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {InputMaskModule} from 'primeng/inputmask';

import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';

import {ActorsComponent} from './actors/actors.component';
import {ActorComponent} from './actor/actor.component';
import {LoginComponent} from './login/login.component';

import {HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import {reducers, metaReducers} from './reducers';
import {EffectsModule} from '@ngrx/effects';
import {AppEffects} from './app.effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {RouterState, StoreRouterConnectingModule} from '@ngrx/router-store';

@NgModule({
  declarations: [
    LoginComponent,
    ActorComponent,
    AppComponent
  ],
  imports: [
    PanelModule,
    InputTextModule,
    FormsModule,
    PasswordModule,
    ButtonModule,
    TableModule,
    InputMaskModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {path: '', component: LoginComponent, loadChildren: () => import('./actors/actors.module').then(m => m.ActorsModule)},
      {path: 'actors', loadChildren: () => import('./actors/actors.module').then(m => m.ActorsModule)},
      {path: 'actor', component: ActorComponent},
    ]),
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([AppEffects]),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    StoreRouterConnectingModule.forRoot({routerState: RouterState.Minimal})
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
