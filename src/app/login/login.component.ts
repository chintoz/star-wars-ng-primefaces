import { Component } from '@angular/core';
import { Login } from './domain/login'
import { Router } from '@angular/router';
import {ActorService} from '../actors/store/actor.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  login = new Login()
  loginError = false;

  constructor(
    private router: Router,
    private actorService: ActorService
  ) { }

  clear() {
    this.login.userName = null;
    this.login.password = null;
  }

  doLogin() {
    if (this.login.userName === 'user' && this.login.password === 'password') {
      this.actorService.loadActors();
      this.router.navigate(['/actors']);
      return;
    }
    this.loginError = true;
    this.login.password = null;
  }
}
